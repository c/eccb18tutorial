<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [ECCB18 T13: Modern and scalable tools for efficient analysis of very large metagenomic datasets](#eccb18-t13-modern-and-scalable-tools-for-efficient-analysis-of-very-large-metagenomic-datasets)
    - [Access your personal de.NBI Cloud virtual machine](#access-your-personal-denbi-cloud-virtual-machine)
    - [Download the tutorial dataset](#download-the-tutorial-dataset)
    - [Run the workflow](#run-the-workflow)
- [Hands-on: Building your own CWL workflow](#hands-on-building-your-own-cwl-workflow)
    - [Introduction](#introduction)
        - [The Common Workflow Language](#the-common-workflow-language)
        - [The Docker Container Engine](#the-docker-container-engine)
    - [The tutorial workflow](#the-tutorial-workflow)
        - [Tools](#tools)
    - [Extend the workflow](#extend-the-workflow)
        - [Task 1: Add a second assembler (Velvet)](#task-1-add-a-second-assembler-velvet)
        - [Task 2: Integrate an assembly evaluation step (MetaQUAST)](#task-2-integrate-an-assembly-evaluation-step-metaquast)
        - [Task 3: Wrap and add a third assembler (SPAdes) _(optional)_](#task-3-wrap-and-add-a-third-assembler-spades-_optional_)
    - [Take a look at the results](#take-a-look-at-the-results)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# ECCB18 T13: Modern and scalable tools for efficient analysis of very large metagenomic datasets

## Access your personal de.NBI Cloud virtual machine

As metagenome assemblies require a lot of compute resources, we will run the tutorial
on the [de.NBI Cloud infrastructure](https://cloud.denbi.de).

Each workshop participant will log into a dedicated _virtual machine_ (VM) and
run all jobs on this machine.

The VM's operating system is _Ubuntu 18.04_ and it has 8 CPU cores,
32GB of RAM and 100GB of hard disk space.

Hopefully, you have already been assigned your `VM ID` (a number between 1 and 128).
If not, please go ahead and ask for one!

1. Go to this page: <a href="https://llil.de" target="_blank">llil.de</a>

1. To access your personal VM, click on your `VM ID`.

1. Enter the username and password of the tutorial.


Afterwards, you will be presented with a web-based text editor for editing files
as well as a terminal window for command line access.


The files view on the left should list a folder called `eccb18tutorial`
containing all needed materials for this tutorial.

Feel free to take at look at the contents of the folder by clicking on the small
triangle next to it.

The complete folder is also available [here](https://gitlab.ub.uni-bielefeld.de/c/eccb18tutorial).

## Download the tutorial dataset

We have prepared a small toy dataset for this tutorial. You can download
the data set by executing the following command inside the terminal window of your VM:

```bash
~/eccb18tutorial/workflow/download_input_files.sh
```

This will create a subdirectory named `eccb18tutorial/input` containing the tutorial dataset.

The tutorial directory has the following structure:

```
eccb18tutorial/
├── docs/            = this document
├── input/           = input data directory
│   ├── genomes/     = reference genomes (used for assembly evaluation)
│   ├── read1.fq.gz  = Read 1 of paired reads (gzipped FASTQ)
│   └── read2.fq.gz  = Read 2 of paired reads (gzipped FASTQ)
├── output/          = directory for workflow outputs (after execution)
└── workflow/        = directory containing the workflow files
```

## Run the workflow

Please switch to the terminal window and use `cwltool` to execute the workflow.
Supply the workflow description file and the
job input description file as arguments. Additionally, specify a cache directory,
an output directory and increase the amount of information cwltool will print
by adding the debug flag.

Command to run the workflow:
```bash
cd ~/eccb18tutorial/workflow/
cwltool --debug --outdir ../output/ --cachedir /tmp/cache/ main.cwl job.yml
```

# Hands-on: Building your own CWL workflow
## Introduction

When the number of analyses to perform increases,
any given data analysis workflow tends to get more complicated, harder to maintain
and reproducibility might suffer as a result.

This can be combated early on by enforcing a certain level of standardization.

This hands-on tutorial will introduce you to _Docker_ as a way to standardize your
workflow's tools as well as the _Common Workflow Language_ to also standardize the
way these tools are invoked and interact in order to produce the desired
output.

#### The YAML file format

The YAML file format is a text file format commonly used for configuration
files. This format is also the preferred way of writing workflows in the
_Common Workflow Language_.

Some of the basic elements of the format are shown below.
Please be aware that the format relies heavily on indentation.

##### Plain key/value pairs (scalars), e.g.:
```yaml
stdout: "fastqc.log"
```
The key `stdout` has the value `fastqc.log`

##### Nested objects, e.g.:
```yaml
inputs:
  fastqForward: File
  fastqReverse: File
  referenceGenomes: File[]
```
The key `inputs` has multiple named properties, one of them being `fastqForward`
which itself is a key/value pair.

##### Sequence of values, e.g.:
```yaml
mykey:
  - "foo"
  - 123
```
The sequence named `mykey` contains two values of which one is of type `string` and the 
other is of type `number`.

##### Sequence of objects, e.g.:
```yaml
referenceGenomes:
  - class: File
    path: ../input/genomes/Aquifex_aeolicus_VF5.fna
  - class: File
    path: ../input/genomes/Bdellovibrio_bacteriovorus_HD100.fna
  - class: File
    path: ../input/genomes/Chlamydia_psittaci_MN.fna
```
The sequence named `referenceGenomes` contains three items that represent objects
with each one having the properties `class` and `path`.

### The Common Workflow Language

The Common Workflow Language (CWL) is a specification for describing analysis
workflows and tools in a way that makes them portable and scalable across a
variety of software and hardware environments.

This tutorial will use the 
[CWL reference implementation](https://github.com/common-workflow-language/cwltool) 
called `cwltool` to execute workflows. A list of other existing 
implementations is available at [commonwl.org](https://www.commonwl.org/).

There is also a high quality CWL user guide available at
[commonwl.org/user_guide](https://www.commonwl.org/user_guide) but due to
time constraints we will try to give you the gist of it below.

#### Basics

A typical CWL workflow consists of a workflow description (`main.cwl`), a
job input description (`job.yml`) and one or more tool descriptions
(e.g. `megahit.cwl`). All these files are written in YAML.

Among other things, a tool description specifies the inputs and outputs of
a specific tool. This information can then be used in the workflow description
to chain the tools together forming a pipeline.
The workflow description also specifies the inputs and outputs of the
workflow itself.

Workflow and tool descriptions do not contain any information on the concrete
data that is to be processed during a specific workflow invocation (job).
The user places this information inside the job input description (`job.yml`).
This way, the whole workflow can be reused by simply substituting the job input
description with one describing a different set of input data.

For demonstration purposes find below a minimal CWL workflow with the sole task
of creating a single directory.

##### Example tool description

`mkdir.cwl`:
```yaml
cwlVersion: v1.0  # the version of CWL this document conforms to
class: CommandLineTool  # declares this file as a tool description
hints:
  DockerRequirement:  # tells the CWL implementation to run the tool inside a Docker container
    dockerPull: alpine  # the Docker image that contains the tool ('alpine' is a very small linux)
baseCommand: mkdir  # the command that invokes the tool
arguments:  # static arguments for the tool
  - position: 1  # where to put the argument on the resulting command line
    valueFrom: '-p'
inputs:  # dynamic arguments for the tool
  dirname:  # handle for a dynamic argument (used inside the workflow description)
    type: string  # data type of the argument
    inputBinding:  #       (most common types are: string, int, File, Directory or File[])
      position: 2  # where to put the argument on the resulting command line
outputs:  # outputs that are to be collected after the tool has finished
  dir:  # handle for a specific output item
    type: Directory  # data type of the output
    outputBinding:  # identifies the files or directories to be collected for this output
      glob: '$(inputs.dirname)'  #       (shell glob syntax can be used, e.g. '*.fa')
```

This will generate the following command line:
```
mkdir -p <dirname>
```

##### Example workflow description

`main.cwl`:
```yaml
cwlVersion: v1.0  # the version of CWL this document conforms to
class: Workflow  # declares this file as a workflow description

inputs:  # the inputs for the workflow
  directoryName: string  # 

outputs:  # the outputs of the workflow
  resultingDir:  #  arbitrary name for this output item
    type: File  # type of this workflow output item
    outputSource: createDirectory/dir  # where in the workflow to get the data from

steps:  # the individual steps of the workflow
  createDirectory:  # name of the workflow step (other steps and outputs can reference this)
    run: mkdir.cwl  # CWL tool description to execute for this step
    in:  # arguments to supply this tool with
      dirname: directoryName
    out:  # selection of output to use from this tool
      - dir

  # more workflow steps can be added here (please ensure correct indentation)
```

##### Example job input description

`job.yml`:
```yaml
directoryName: "mydir"
```

### The Docker Container Engine

The Docker container engine makes a piece of software and its whole
environment portable in the form of Docker images.
These images can be downloaded and executed anywhere and will yield
the same results independent of the underlying system where the
Docker container engine is installed.
Therefore they are the ideal instrument for tasks that have to be
100% reproducible like e.g. scientific analysis workflows.
Of course, the workflow we will be working on today consists entirely
of Docker containers doing the computations.

#### Basics

For now, we only need to know about the one Docker feature that enables us
to start a _Docker container_ based on a publicly available _Docker image_
and run an executable inside this container. The command is:
```bash
docker run image_id:tag executable
```

Think of a Docker image as a portable snapshot file of a minimal operating
system (usually a linux system) where the tool you want to use is
already pre-installed and ready to run.
Often, you can choose between lots of publicly available images
(`image_id`) and tool versions (`tag`).
Images can be downloaded either from the official
[Docker Hub](https://hub.docker.com/) or from other sources like e.g.
[quay.io](https://quay.io).

When using `docker run`, the specified image will be downloaded and
a Docker container will be created from the image.
The Docker container is equivalent to a small computer that has been
started using the operating system that is inside the image.

Because our tool of interest is already installed, we can download the
image, create the container and run the tool in one go, e.g. we can run
the _SPAdes assembler_ using:

```bash
docker run --rm -ti quay.io/biocontainers/spades:3.12.0--1 spades.py
```

We can even tell Docker to delete the container once the executable
is finished (`--rm`) and that we want to be able to interact
with it on the command line (`-ti`) while it is running.

Because the Docker container has its own filesystem that is
completely separate from the filesystem of the host,
data transfer is achieved through shared directories, e.g.:

```bash
docker run [...] -v /hostdir/:/containerdir/ [...]
```

For a much broader introduction there is an official
[Docker user guide](https://docs.docker.com/get-started/).

## The tutorial workflow

The tutorial workflow contains a set of tools suited for the analysis
of large metagenomic datasets. The diagram below shows the relationships
and the data flow between those programs as well as which programs
contribute to the final output.

![workflow-diagram](workflow.png)

### Tools

#### FastQC
FastQC aims to provide a simple way to do some quality control checks
on raw sequence data coming from high throughput sequencing
pipelines. It provides a modular set of analyses which you can use to
give a quick impression of whether your data has any problems of which
you should be aware before doing any further analysis.

The main functions of FastQC are

* Import of data from BAM, SAM or FastQ files (any variant)
* Providing a quick overview to tell you in which areas there may be problems
* Summary graphs and tables to quickly assess your data
* Export of results to an HTML based permanent report
* Offline operation to allow automated generation of reports without running the interactive application

See the [FastQC home page](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/>) for more info.

This is the command to run ``FastQC`` on two FASTQ files:

```bash
fastqc read1.fq read2.fq
```

After ``FastQC`` finished running, you can check the HTML reports 
in the files ``read1_fastqc.html`` and ``read1_fastqc.html``.

#### MEGAHIT
MEGAHIT is a single node assembler for large and complex metagenomics
NGS reads, such as soil. It makes use of succinct de Bruijn graph
(SdBG) to achieve low memory assembly. MEGAHIT can optionally utilize
a CUDA-enabled GPU to accelerate its SdBG contstruction. See the
[MEGAHIT home page](https://github.com/voutcn/megahit/>) for more
info.

MEGAHIT can be run by the following command. As our compute instance
have multiple cores, we can use the option `-t 14` to tell MEGAHIT it
should use 14 parallel threads. The output will be redirected to file
`megahit.log`::

```bash
megahit -1 read1.fq -2 read2.fq -t 14 -o megahit_out
```

The contig sequences will be located in the `megahit_out` directory in
file `final.contigs.fa`. 

#### Velvet

Velvet was one of the first de novo genomic assemblers specially
designed for short read sequencing technologies. It was developed by
Daniel Zerbino and Ewan Birney at the European Bioinformatics
Institute (EMBL-EBI). Velvet currently takes in short read sequences,
removes errors then produces high quality unique contigs. It then uses
paired-end read and long read information, when available, to retrieve
the repeated areas between contigs. See the [Velvet GitHub page]
(https://github.com/dzerbino/velvet>) for more info.

Velvet runs are devided into two separate steps for the assembly. 
``velveth`` takes in a number of sequence files, produces a hashtable, then
outputs two files in an output directory (creating it if necessary), Sequences
and Roadmaps, which are necessary for running ``velvetg`` in the next step.

Example:

```bash
velveth velvet_31 31 -shortPaired -fastq -separate read1.fq read2.fq
```  

Now we have to start the actual assembly using ``velvetg``. ``velvetg`` is the 
core of Velvet where the de Bruijn graph is built then manipulated. 
See the [Velvet manual](https://github.com/dzerbino/velvet/blob/master/Manual.pdf) 
for more info about parameter settings. 

```bash
velvetg velvet_31 -cov_cutoff auto -ins_length 270 -min_contig_lgth 500 -exp_cov auto
```  

#### SPAdes

SPAdes – St. Petersburg genome assembler – is an assembly toolkit containing various 
assembly pipelines. The current version of SPAdes works with Illumina or IonTorrent 
reads and is capable of providing hybrid assemblies using PacBio, Oxford Nanopore 
and Sanger reads. You can also provide additional contigs that will be used as long reads.
Check the [SPAdes Homepage](http://cab.spbu.ru/software/spades/) for more information and [see the manual]
(http://cab.spbu.ru/files/release3.12.0/manual.html) for details
about running SPAdes.

#### Prodigal

Prodigal (Prokaryotic Dynamic Programming Genefinding Algorithm) is a
microbial (bacterial and archaeal) gene finding program developed at
Oak Ridge National Laboratory and the University of Tennessee. See the
[Prodigal home page](http://prodigal.ornl.gov/>) for more info.

To run ``prodigal`` on the command line, you would type::

```bash
prodigal -p meta -a final.contigs.genes.faa -d final.contigs.genes.fna -f gff -o final.contigs.genes.gff -i final.contigs.fa
```

Output files:

| Output file             | Content                                     |
|-------------------------|---------------------------------------------|
| final.contigs.genes.gff | positions of predicted genes in GFF format  |
| final.contigs.genes.faa | protein translations of predicted genes     |
| final.contigs.genes.fna | nucleotide sequences of predicted genes     |


#### BBmap

BBMap: Short read aligner for DNA and RNA-seq data. Capable of
handling arbitrarily large genomes with millions of scaffolds. Handles
Illumina, PacBio, 454, and other reads; very high sensitivity and
tolerant of errors and numerous large indels. Very fast. See the
[BBTools home page](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/) 
for more info.

#### MetaBAT

MetaBAT, An Efficient Tool for Accurately Reconstructing Single
Genomes from Complex Microbial Communities.

Grouping large genomic fragments assembled from shotgun metagenomic
sequences to deconvolute complex microbial communities, or metagenome
binning, enables the study of individual organisms and their
interactions. MetaBAT is an automated metagenome binning software
which integrates empirical probabilistic distances of genome abundance
and tetranucleotide frequency. See the [MetaBAT home page]
(https://bitbucket.org/berkeleylab/metabat>) for more info.

## Extend the workflow

Your task is now to extend the workflow with alternative assembly tools 
as well as an assembly evaluation step that compares the quality of the 
different assemblies. The required extensions are highlighted in purple
in the diagram below.

![extended-workflow-diagram](extended-workflow.png)

### Task 1: Add a second assembler (Velvet)

Our main workflow file `main.cwl` contains a list of steps that describe
which tools are to be run. Adding another tool usually means adding another step
to the list and connecting the `in:` and `out:` to the rest of the workflow.

Your first task is now to add a second assembler (Velvet) to the mix, giving us the
opportunity to benchmark it against the already integrated MEGAHIT and see which
one performs better.

_Hint:_ When we compare the already integrated `megahit.cwl` and the file
`velvet.cwl`, we notice that they have identical inputs and outputs and
therefore might be integrated into the workflow in a similar manner.
You should also consider adding the Velvet output to the list of desired
workflow output files.

#### Run the updated workflow

Make sure to save any changes you have made to the workflow files
by clicking `File -> Save All`.

Now run the workflow again using our previous `cwltool` command
(see _[Run the workflow](#run-the-workflow)_ above).
Once it finishes there should be a new file called `velvet.contigs.fa` inside
your `output` directory.


### Task 2: Integrate an assembly evaluation step (MetaQUAST)

Your second task will be to add another step to the workflow that
runs an evaluation on our two assemblies and generates a report
page for us.

Copy the step description below into your workflow and make sure
that all lines have the correct indentation. Then, fill in the assemblies
and capture the resulting output in the main `outputs:` section,
keeping in mind that the MetaQUAST `report` output is of type `Directory`.

```yaml
  evaluateAssemblies:
    label: 'evaluate assemblies'
    run: tools/metaquast.cwl
    in:
      references: referenceGenomes
      labels:
        default:
          - 'megahit'
          - ...
      assemblies:
        - assembleMegahit/assembly
        - ...
    out:
      - report
```

#### Run the updated workflow

Make sure to save any changes you have made to the workflow files
by clicking `File -> Save All`.

Now run the workflow again using our previous `cwltool` command
(see _[Run the workflow](#run-the-workflow)_ above).
Once it finishes there should be a new directory called `metaquast`
inside your `output` directory.

To view the assembly evaluation results MetaQUAST has produced,
go to the file tree view and expand the `output` directory and 
from there expand the directory `metaquast`.
Next, right-click on the file named `report.html` 
and select `Preview` from the context menu.
The results will be displayed in a new tab inside your VM.

### Task 3: Wrap and add a third assembler (SPAdes) _(optional)_

While we were lucky that someone already wrote the file `velvet.cwl`
for us, there definitely will be some tools out there that have not yet
been conveniently wrapped inside a CWL tool description ready for us to use.

Whenever this is the case, a closer look at the tool and its invocation
options becomes necessary.

As the workflow runs all the tools we use inside Docker containers, 
we will also go ahead and use Docker for our manual inspection of
the new tool we intend to add.

The public Docker image repository
[quay.io/organization/biocontainers](https://quay.io/organization/biocontainers)
contains a number of pre-built Docker images for all sorts of
bioinformatic tools. Go ahead and filter the page for `spades`, click on
the result and then on the _Tags_ menu item on the left.
You should now see [this page](https://quay.io/repository/biocontainers/spades?tab=tags)
listing all the available software versions for the SPAdes assembler.
For our workflow we will use version `3.12.0--1`.

The resulting Docker image ID consists of the following elements:

| image repository       | container name | version tag      |
|------------------------|----------------|------------------|
| quay.io/biocontainers/ | spades         | :3.12.0--1       |

To do a test launch we take a quick look at the official
[SPAdes manual](http://cab.spbu.ru/files/release3.12.0/manual.html#sec3.2)
to find out the name of the executable we have to use:

>To run SPAdes from the command line, type
>```
>     spades.py [options] -o <output_dir>
>```

The resulting command line to launch a temporary container from this
image looks like this:
```
docker run --rm -ti quay.io/biocontainers/spades:3.12.0--1 spades.py -h
```
With the parameter `-h`, the tool prints a help page including a
list of available command line options.

Feel free to use both the manual page and the command line help
to write your own CWL tool description around SPAdes.
Create a new file called `spades.cwl` inside the directory
`~/eccb18tutorial/tools/assemblers/`.
We recommend to use `megahit.cwl` as a template.


_Important:_ When constructing the SPAdes command line arguments,
please keep in mind that we process **metagenomic data** (`--meta`)
and that we want to use **multiple threads**. 
To be able to postprocess the resulting assembly later down the line
(e.g. for comparison) you will 
also need to make sure that you **mark the contigs file as an output file**
inside the tool description.
You may consult the SPAdes manual in the section
_SPAdes output_ ([direct link](http://cab.spbu.ru/files/release3.12.0/manual.html#sec3.5))
to see how SPAdes organizes its output and where it **puts the final contigs**.

Once you've finished building your wrapper, you can integrate it
into the existing workflow the same way you did with Velvet
(don't forget to add its assembly to the `outputs:`).

Finally you can add the SPAdes output to the **assembly evaluation step**
to get a neat comparison of all three assemblers.

#### Run the updated workflow

Make sure to save any changes you have made to the workflow files
by clicking `File -> Save All`.

Now run the workflow again using our previous `cwltool` command
(see _[Run the workflow](#run-the-workflow)_ above).
Once it finishes there should be a new file called `contigs.fasta`
containing the SPAdes assembly inside your `output` directory.

Take a look at the updated MetaQUAST report to compare the performance
of SPAdes with the other assemblers.

## Take a look at the results

All data that has been marked inside the workflow as _to be included
into the output directory_ by specifying it in the `outputs:` section of 
the workflow will be written to the designated output directory given to 
`cwltool` through the use of `--outdir`. In our case the output directory 
is located at `~/eccb18tutorial/output/`.

Depending on the size of the output files you might want to view the files
using the command line e.g. via `less <filename>` because they might be
to big to be displayed inside the text editor.

Some of the tools like e.g. FastQC generate reports in `.html` format.
To view the results FastQC has produced, go to the file 
tree view and expand the `output` directory.
Next, right-click on the file named `read1_fastqc.html` 
and select `Preview` from the context menu.
The results will be displayed in a new tab inside your VM.
The same goes for the file `read2_fastqc.html`.