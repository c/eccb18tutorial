cwlVersion: v1.0
class: ExpressionTool
requirements:
- class: InlineJavascriptRequirement
inputs:
  bamFile:
    type: File
  bamIndexFile: File
outputs:
  bamWithIndex: File
expression: '${ inputs.bamFile.secondaryFiles = [ inputs["bamIndexFile"] ]; return { "bamWithIndex": inputs.bamFile }; }'