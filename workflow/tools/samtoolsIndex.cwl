cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 1
    ramMin: 1024
    tmpdirMin: 1024
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/samtools:1.3.1--5
label: samtools 1.3.1
baseCommand: samtools
arguments:
  - "index"
  - valueFrom: "$(inputs.bamFile.basename).bai"
    position: 2
inputs:
  bamFile:
    type: File
    inputBinding:
      position: 1
outputs:
  index:
    type: File
    outputBinding:
      glob: "$(inputs.bamFile.basename).bai"