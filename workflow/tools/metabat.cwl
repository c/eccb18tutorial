cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 14
hints:
  DockerRequirement:
    dockerPull: virusx/metabat:2.12.1
label: MetaBAT 2.12.1

baseCommand: metabat2

arguments:
  - position: 3
    prefix: '--outFile'
    valueFrom: 'bins/bin'

inputs:
  assembly:
    type: File
    inputBinding:
      position: 4
      prefix: '-i'
  depths:
    type: File
    inputBinding:
      position: 5
      prefix: '-a'

outputs:
  bins:
    type: Directory
    outputBinding:
      glob: "bins"
  log:
    type: stdout

stdout: "metabat.log"
stderr: "metabat.log"