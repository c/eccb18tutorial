cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 48
    ramMin: 24000
    tmpdirMin: 4096
hints:
  DockerRequirement:
    dockerPull: virusx/bbmap:36.84--0
label: BBMap 36.84
baseCommand: bbmap.sh
arguments:
  - "-Xmx$(runtime.ram)M"
  - "nodisk=t"
  - "out=$(inputs.reference.basename).sam"
  - "statsfile=$(inputs.reference.basename).stats"
  - "covstats=$(inputs.reference.basename).covstats"
  - "rpkm=$(inputs.reference.basename).rpkm"
inputs:
  forward:
    type: File
    inputBinding:
      position: 1
      prefix: 'in='
      separate: false
  reverse:
    type: File?
    inputBinding:
      position: 2
      prefix: 'in2='
      separate: false
  reference:
    type: File
    inputBinding:
      position: 3
      prefix: 'ref='
      separate: false
outputs:
  sam:
    type: stdout
  stats:
    type: File
    outputBinding:
      glob: "$(inputs.reference.basename).stats"
  covstats:
    type: File
    outputBinding:
      glob: "$(inputs.reference.basename).covstats"
  rpkm:
    type: File
    outputBinding:
      glob: "$(inputs.reference.basename).rpkm"

stdout: '$(inputs.reference.basename).sam'