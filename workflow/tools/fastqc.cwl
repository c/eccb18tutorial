cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 24
    ramMin: 8000
    tmpdirMin: 4096
hints:
  DockerRequirement:
    dockerPull: virusx/fastqc:0.11.7
label: FastQC
baseCommand: fastqc
arguments:
  - position: 1
    prefix: '-t'
    valueFrom: '$(runtime.cores)'
  - position: 2
    prefix: '-o'
    valueFrom: '$(runtime.outdir)'
inputs:
  fastqs:
    type: File[]
    inputBinding:
      position: 5
outputs:
  analysis:
    type: File[]
    outputBinding:
      glob: "*.html"
  log:
    type: stdout
stdout: "fastqc.log"
stderr: "fastqc.log"
