cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 14
    ramMin: 12000
    tmpdirMin: 4096
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/quast:5.0.0--py27pl526ha92aebf_0
label: MetaQUAST 5.0.0
baseCommand: metaquast.py
arguments:
  - position: 1
    prefix: '--threads'
    valueFrom: '$(runtime.cores)'
  - position: 2
    valueFrom: '--gene-finding'
  - position: 4
    prefix: '-o'
    valueFrom: 'metaquast'

inputs:
  references:
    type: File[]
    inputBinding:
      position: 5
      prefix: '-R'
      itemSeparator: ','
  labels:
    type: string[]
    inputBinding:
      position: 6
      prefix: '-l'
      itemSeparator: ','
  assemblies:
    type: File[]
    inputBinding:
      position: 7
outputs:
  report:
    type: Directory
    outputBinding:
      glob: metaquast
  log:
    type: stdout
stdout: "metaquast.log"
stderr: "metaquast.log"
