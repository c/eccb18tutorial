cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 14
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/trimmomatic:0.38--0
label: Trimmomatic 0.38

baseCommand: trimmomatic

arguments:
  - position: 1
    valueFrom: 'PE'
  - position: 2
    prefix: '-baseout'
    valueFrom: 'trimmomatic.fastq.gz'
  - position: 3
    prefix: '-threads'
    valueFrom: '$(runtime.cores)'

inputs:
  forward:
    type: File
    inputBinding:
      position: 4
  reverse:
    type: File
    inputBinding:
      position: 5
  headcrop:
    type: int?
    inputBinding:
      position: 6
      prefix: 'HEADCROP:'
      separate: false
  illuminaclip:
    type: string
    inputBinding:
      position: 7
      prefix: 'ILLUMINACLIP:/usr/local/share/trimmomatic-0.38-0/adapters/'
      separate: false
  illuminaclip2:
    type: string?
    inputBinding:
      position: 8
      prefix: 'ILLUMINACLIP:/usr/local/share/trimmomatic-0.38-0/adapters/'
      separate: false
  illuminaclip3:
    type: string?
    inputBinding:
      position: 9
      prefix: 'ILLUMINACLIP:/usr/local/share/trimmomatic-0.38-0/adapters/'
      separate: false
  crop:
    type: int?
    inputBinding:
      position: 10
      prefix: 'CROP:'
      separate: false
  leading:
    type: int?
    inputBinding:
      position: 11
      prefix: 'LEADING:'
      separate: false
  trailing:
    type: int?
    inputBinding:
      position: 12
      prefix: 'TRAILING:'
      separate: false
  slidingwindow:
    type: string?
    inputBinding:
      position: 13
      prefix: 'SLIDINGWINDOW:'
      separate: false
  minlen:
    type: int?
    inputBinding:
      position: 14
      prefix: 'MINLEN:'
      separate: false

outputs:
  forwardPaired:
    type: File[]
    format: 'fastq.gz'
    outputBinding:
      glob: "trimmomatic_1P.fastq.gz"
  reversePaired:
    type: File[]
    format: 'fastq.gz'
    outputBinding:
      glob: "trimmomatic_2P.fastq.gz"
  forwardUnpaired:
    type: File
    format: 'fastq.gz'
    outputBinding:
      glob: "trimmomatic_1U.fastq.gz"
  reverseUnpaired:
    type: File
    format: 'fastq.gz'
    outputBinding:
      glob: "trimmomatic_2U.fastq.gz"
  log:
    type: stderr

stderr: trimmomatic.log
