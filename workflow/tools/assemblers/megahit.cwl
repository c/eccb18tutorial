cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 14
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/megahit:1.1.3--py35_0
label: MEGAHIT 1.1.3

baseCommand: megahit

arguments:
  - position: 1
    prefix: '-t'
    valueFrom: '$(runtime.cores)'
  - position: 2
    prefix: '--out-prefix'
    valueFrom: 'megahit'  # MEGAHIT will append '_out' to this value resulting in 'megahit_out' as the output directory

inputs:
  forward:
    type: File
    inputBinding:
      position: 3
      prefix: '-1'
  reverse:
    type: File
    inputBinding:
      position: 4
      prefix: '-2'

outputs:
  assembly:
    type: File
    outputBinding:
      glob: megahit_out/megahit.contigs.fa
  log:
    type: File
    outputBinding:
      glob: megahit_out/megahit.log
