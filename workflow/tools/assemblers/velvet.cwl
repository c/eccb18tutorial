cwlVersion: v1.0
class: Workflow
label: ECCB18 tutorial - metagenomics pipeline
requirements:
  - class: ScatterFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
  - class: SubworkflowFeatureRequirement

inputs:
  forward: File
  reverse: File

outputs:
  assembly:
    type: File
    outputSource: renameAssemblyFile/file
  log:
    type: File
    outputSource: velvetg/log

steps:
  unzipForward:
    label: 'unzip forward reads'
    in:
      gz: forward
    out:
      - file
    run:
      cwlVersion: v1.0
      class: CommandLineTool
      requirements:
        - class: ResourceRequirement
          coresMin: 1
          ramMin: 512
      hints:
        DockerRequirement:
          dockerPull: alpine
      baseCommand: gunzip
      arguments:
        - "-c"
      inputs:
        gz:
          type: File
          inputBinding:
            position: 1
      outputs:
        file:
          type: stdout
      stdout: "$(inputs.gz.nameroot)"

  unzipReverse:
    label: 'unzip reverse reads'
    in:
      gz: reverse
    out:
      - file
    run:
      cwlVersion: v1.0
      class: CommandLineTool
      requirements:
        - class: ResourceRequirement
          coresMin: 1
          ramMin: 512
      hints:
        DockerRequirement:
          dockerPull: alpine
      baseCommand: gunzip
      arguments:
        - "-c"
      inputs:
        gz:
          type: File
          inputBinding:
            position: 1
      outputs:
        file:
          type: stdout
      stdout: "$(inputs.gz.nameroot)"

  velveth:
    label: 'generate contigs hashtable'
    in:
      forward: unzipForward/file
      reverse: unzipReverse/file
    out:
      - velvetDir
    run:
      cwlVersion: v1.0
      class: CommandLineTool
      requirements:
        - class: ResourceRequirement
          coresMin: 14
      hints:
        DockerRequirement:
          dockerPull: quay.io/biocontainers/velvet:1.2.10--1
      label: Velvet 1.2.10

      baseCommand: velveth

      arguments:
        - position: 1
          valueFrom: 'velvet_out'
        - position: 2
          valueFrom: '31'
        - position: 3
          valueFrom: '-fastq'
        - position: 4
          valueFrom: '-separate'
        - position: 5
          valueFrom: '-shortPaired'

      inputs:
        forward:
          type: File
          inputBinding:
            position: 6
        reverse:
          type: File
          inputBinding:
            position: 7

      outputs:
        velvetDir:
          type: Directory
          outputBinding:
            glob: velvet_out
        log:
          type: stdout

      stdout: velveth.log
      stderr: velveth.log

  velvetg:
    label: 'assemble contigs'
    in:
      velvetDir: velveth/velvetDir
    out:
      - assembly
      - log
    run:
      cwlVersion: v1.0
      class: CommandLineTool
      requirements:
        - class: ResourceRequirement
          coresMin: 14
        - class: InitialWorkDirRequirement
          listing:
            - entry: '$(inputs.velvetDir)'
              writable: true
      hints:
        DockerRequirement:
          dockerPull: quay.io/biocontainers/velvet:1.2.10--1
      label: Velvet 1.2.10

      baseCommand: velvetg

      arguments:
        - position: 1
          valueFrom: '$(inputs.velvetDir.basename)'
        - position: 3
          prefix: '-cov_cutoff'
          valueFrom: 'auto'
        - position: 4
          prefix: '-ins_length'
          valueFrom: '270'
        - position: 5
          prefix: '-min_contig_lgth'
          valueFrom: '500'
        - position: 6
          prefix: '-exp_cov'
          valueFrom: 'auto'

      inputs:
        velvetDir:
          type: Directory

      outputs:
        assembly:
          type: File
          outputBinding:
            glob: '$(inputs.velvetDir.basename)/contigs.fa'
        log:
          type: stdout

      stdout: velvet.log
      stderr: velvet.log

  renameAssemblyFile:
    label: 'rename assembly file'
    in:
      oldFile: velvetg/assembly
      newFilename:
        default: velvet.contigs.fa
    out:
      - file
    run:
      cwlVersion: v1.0
      class: ExpressionTool
      requirements:
        - class: InlineJavascriptRequirement
      inputs:
        oldFile: File
        newFilename: string
      outputs:
        file: File
      expression: |
        ${
          inputs.oldFile.basename = inputs.newFilename;
          return { "file": inputs.oldFile };
        }