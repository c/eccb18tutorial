cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 4
    ramMin: 8192
    tmpdirMin: 8192
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/samtools:1.3.1--5
label: samtools 1.3.1
baseCommand: samtools
arguments:
  - "view"
inputs:
  inputFile:
    type: File
    streamable: true
  outputBam:
    type: boolean
    inputBinding:
      prefix: '-b'
  uncompressed:
    type: boolean
    inputBinding:
      prefix: '-u'
  outputFilename:
    type: string
outputs:
  stdout:
    type: stdout
stdin: '$(inputs.inputFile.path)'
stdout: '$(inputs.outputFilename)'