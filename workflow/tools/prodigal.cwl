cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 14
    ramMin: 12000
    tmpdirMin: 4096
hints:
  DockerRequirement:
    dockerPull: quay.io/biocontainers/prodigal:2.6.3--0
label: Prodigal 2.6.3
baseCommand: prodigal
arguments:
  - "-p"
  - "meta"
  - "-f"
  - "gff"
  - "-o"
  - "prodigal.gff"
  - "-a"
  - "prodigal.faa"
  - "-d"
  - "prodigal.fna"
inputs:
  inputFile:
    type: File
    inputBinding:
      position: 1
      prefix: '-i'
outputs:
  annotations:
    type: File
    outputBinding:
      glob: prodigal.gff
  genes:
    type: File
    outputBinding:
      glob: prodigal.fna
  proteins:
    type: File
    outputBinding:
      glob: prodigal.faa
  log:
    type: stderr
stderr: "prodigal.log"
