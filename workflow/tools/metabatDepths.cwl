cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: ResourceRequirement
    coresMin: 48
    ramMin: 24000
    tmpdirMin: 4096
hints:
  DockerRequirement:
    dockerPull: virusx/metabat:2.12.1
label: MetaBAT 2.12.1
baseCommand: jgi_summarize_bam_contig_depths
arguments:
  - position: 1
    prefix: '--outputDepth'
    valueFrom: 'metabat.depth.txt'
inputs:
  bamFile:
    type: File
    inputBinding:
      position: 2
outputs:
  depths:
    type: File
    outputBinding:
      glob: "metabat.depth.txt"
  log:
    type: stdout
stdout: "metabat.depth.log"
stderr: "metabat.depth.log"