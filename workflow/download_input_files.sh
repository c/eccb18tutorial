#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
mkdir -p ${DIR}/../input/
mkdir -p ${DIR}/../input/genomes/

wget -O ${DIR}/../input/read1.fastq.gz "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/read1.fastq.gz"
wget -O ${DIR}/../input/read2.fastq.gz "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/read2.fastq.gz"

wget -O ${DIR}/../input/genomes/Aquifex_aeolicus_VF5.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Aquifex_aeolicus_VF5.fna"
wget -O ${DIR}/../input/genomes/Bdellovibrio_bacteriovorus_HD100.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Bdellovibrio_bacteriovorus_HD100.fna"
wget -O ${DIR}/../input/genomes/Chlamydia_psittaci_MN.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydia_psittaci_MN.fna"
wget -O ${DIR}/../input/genomes/Chlamydophila_pneumoniae_CWL029.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydophila_pneumoniae_CWL029.fna"
wget -O ${DIR}/../input/genomes/Chlamydophila_pneumoniae_J138.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydophila_pneumoniae_J138.fna"
wget -O ${DIR}/../input/genomes/Chlamydophila_pneumoniae_LPCoLN.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydophila_pneumoniae_LPCoLN.fna"
wget -O ${DIR}/../input/genomes/Chlamydophila_pneumoniae_TW_183.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydophila_pneumoniae_TW_183.fna"
wget -O ${DIR}/../input/genomes/Chlamydophila_psittaci_C19_98.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Chlamydophila_psittaci_C19_98.fna"
wget -O ${DIR}/../input/genomes/Finegoldia_magna_ATCC_29328.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Finegoldia_magna_ATCC_29328.fna"
wget -O ${DIR}/../input/genomes/Fusobacterium_nucleatum_ATCC_25586.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Fusobacterium_nucleatum_ATCC_25586.fna"
wget -O ${DIR}/../input/genomes/Helicobacter_pylori_26695.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Helicobacter_pylori_26695.fna"
wget -O ${DIR}/../input/genomes/Lawsonia_intracellularis_PHE_MN1_00.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Lawsonia_intracellularis_PHE_MN1_00.fna"
wget -O ${DIR}/../input/genomes/Mycobacterium_leprae_TN.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Mycobacterium_leprae_TN.fna"
wget -O ${DIR}/../input/genomes/Porphyromonas_gingivalis_W83.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Porphyromonas_gingivalis_W83.fna"
wget -O ${DIR}/../input/genomes/Wigglesworthia_glossinidia.fna "https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/eccb18tutorial/data/genomes/Wigglesworthia_glossinidia.fna"