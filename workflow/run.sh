#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
mkdir -p ${DIR}/../output/
cwltool --outdir ${DIR}/../output/ --cachedir /tmp/cache/ --debug ${DIR}/main.cwl ${DIR}/job.yml