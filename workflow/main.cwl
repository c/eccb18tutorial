cwlVersion: v1.0
class: Workflow
label: ECCB18 tutorial - metagenomics pipeline
requirements:
  - class: ScatterFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
  - class: SubworkflowFeatureRequirement


###  INPUTS   ################################################
inputs:
  fastqForward: File
  fastqReverse: File
  referenceGenomes: File[]


###  OUTPUTS   ###############################################
outputs:
  fastqcAnalysis:
    type: File[]
    outputSource: fastqc/analysis

  megahitAssembly:
    type: File
    outputSource: assembleMegahit/assembly
  megahitLog:
    type: File
    outputSource: assembleMegahit/log

  predictedGenesAnnotations:
    type: File
    outputSource: predictGenes/annotations
  predictedGenesGenes:
    type: File
    outputSource: predictGenes/genes
  predictedGenesProteins:
    type: File
    outputSource: predictGenes/proteins

  bam:
    type: File
    outputSource: joinBamWithIndex/bamWithIndex
  bins:
    type: Directory
    outputSource: binContigs/bins
  binLog:
    type: File
    outputSource: binContigs/log


###  STEPS   #################################################
steps:
  fastqc:
    label: 'assess quality of reads'
    run: tools/fastqc.cwl
    in:
      fastqs:
        - fastqForward
        - fastqReverse
    out:
      - analysis

  assembleMegahit:
    label: 'assemble reads using MEGAHIT'
    run: tools/assemblers/megahit.cwl
    in:
      forward: fastqForward
      reverse: fastqReverse
    out:
      - assembly
      - log

  predictGenes:
    label: 'predict genes'
    run: tools/prodigal.cwl
    in:
      inputFile: assembleMegahit/assembly
    out:
      - annotations
      - genes
      - proteins
      - log

  mapReadsToAssembly:
    label: 'map reads to assembly'
    run: tools/bbmap.cwl
    in:
      forward: fastqForward
      reverse: fastqReverse
      reference: assembleMegahit/assembly
    out:
      - sam
      - stats
      - covstats
      - rpkm

  convertSamToBam:
    label: 'convert SAM to BAM format'
    run: tools/samtoolsView.cwl
    in:
      inputFile: mapReadsToAssembly/sam
      outputBam:
        default: true
      uncompressed:
        default: true
      outputFilename:
        default: 'megahit.bam'
    out:
      - stdout

  sortBam:
    label: 'sort BAM file'
    run: tools/samtoolsSort.cwl
    in:
      inputFile: convertSamToBam/stdout
    out:
      - bamFile

  indexBam:
    label: 'index BAM file'
    run: tools/samtoolsIndex.cwl
    in:
      bamFile: sortBam/bamFile
    out:
      - index

  joinBamWithIndex:
    run: tools/joinBamWithIndex.cwl
    in:
      bamFile: sortBam/bamFile
      bamIndexFile: indexBam/index
    out:
      - bamWithIndex

  calculateDepthsForBinning:
    label: 'calculate BAM contig depths'
    run: tools/metabatDepths.cwl
    in:
      bamFile: joinBamWithIndex/bamWithIndex
    out:
      - depths

  binContigs:
    label: 'sort contigs into bins'
    run: tools/metabat.cwl
    in:
      assembly: assembleMegahit/assembly
      depths: calculateDepthsForBinning/depths
    out:
      - bins
      - log
